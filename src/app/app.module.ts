import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginModule } from './login-module/login-module.module';
import { HomeModule } from './home-module/home-module.module';
import { UsersModule } from './users-module/users-module.module';
import { UiModule } from './_shared/ui/ui.module';
import { AuthService } from './_auth/services/auth.service';
import { UsersService } from './_services/users.service';
import { AuthGuard } from './_auth/guards/auth.guard';
import { APP_BASE_HREF } from '@angular/common';
import { TokenIntercept } from './_auth/tokenintercept';
import { FakeBackendInterceptor } from './_shared/fakebackend';

import { OrderModule } from 'ngx-order-pipe';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    OrderModule,
    AppRoutingModule,
    UiModule,
    LoginModule,
    HomeModule,
    UsersModule

  ],
  providers: [AuthService,UsersService,AuthGuard,
    { provide: APP_BASE_HREF, useValue: '/'},
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenIntercept,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: FakeBackendInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
