import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersListComponent } from './users-list/users-list.component';

import { Routes, RouterModule } from '@angular/router';
import { OrderModule } from 'ngx-order-pipe';


const routes: Routes = [
  { path: '', component: UsersListComponent },
];

@NgModule({
  declarations: [UsersListComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    OrderModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [RouterModule]
})
export class UsersModule { }
