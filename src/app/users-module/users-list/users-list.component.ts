import { Component, OnInit } from '@angular/core';
import { Observable, Subject, throwError, of, BehaviorSubject } from 'rxjs';

import { UserModel } from '../../_models/user.model';
import { UsersService } from 'src/app/_services/users.service';
import { OrderPipe } from 'ngx-order-pipe';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  items$: BehaviorSubject<UserModel[]>;
  order: string = 'firstName';
  reverse: boolean = false;
  sortedCollection: any[];

  userData:any[];

  

  constructor(
    private orderPipe: OrderPipe,
    private userService: UsersService
  ) {
   
   }

  ngOnInit(): void {
    this.items$  = this.userService.items$;
    this.userService.getPaginatedResults({
        current: 1,
        next: 2,
        previous: 0
      }).subscribe(
        (result) => {
          this.userData = result
          this.sortedCollection = this.orderPipe.transform(this.userData);
        }
      );
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }

  pagination(page) {
    this.userService.getPaginatedResults({
      current: page,
      next: page + 1,
      previous: page - 1
    }).subscribe(
      (result) => {
        this.userData = result
        this.sortedCollection = this.orderPipe.transform(this.userData);
      }
    );
  }

  onSearchUser(search) {
    this.userService.getFilteredData(search).subscribe(
      (result) => {
        console.log(result)
        this.userData = result
        this.sortedCollection = this.orderPipe.transform(this.userData);
      }
    );
  }
}

  


