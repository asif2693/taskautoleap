
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators, AbstractControl, FormBuilder, FormGroup, FormControl , Validator , FormsModule} from '@angular/forms';


import {UserModel} from '../../_models/user.model'
import { CheckRequiredField } from 'src/app/_shared/helpers/form.helper';
import { UsersService } from 'src/app/_services/users.service';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

 
  
  @Input() item: UserModel;
  @Output() formSubmitEvent = new EventEmitter<string>();

  userForm: FormGroup;

  isProcessing: Boolean = false;

  checkField  = CheckRequiredField;

  constructor(
    private userService: UsersService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.userForm = new FormGroup({
      firstName: new FormControl(this.item ? this.item.firstName : '', Validators.required),
      lastName: new FormControl(this.item ? this.item.lastName : '',Validators.required),
      emailAddress: new FormControl(this.item ? this.item.emailAddress : '', [ Validators.required, Validators.email]),
      phoneNo: new FormControl(this.item ? this.item.phoneNo : '',Validators.required),
      address: new FormControl(this.item ? this.item.address : ''),
      zipCode: new FormControl(this.item ? this.item.zipCode : ''),
      countryCode: new FormControl(this.item ? this.item.countryCode : ''),
      // id: new FormControl(this.item ? this.item.id : null),
    });
  }

  getButtonText(): string {
    return  'Add';
  }

  onSubmit($event) {
    this.isProcessing  = true;
    if (this.userForm.valid) {
        if (!this.item) {
          this.doAddUser();
        }
    }
  }

  private doAddUser() {
    this.userService.add(this.userForm.value).subscribe(
      (result) => {
        this.userForm.reset();
        this.formSubmitEvent.next('add');
        this.isProcessing  = false;
      }
    );
  }


  private reset() {
    this.item  = null;
    this.userForm.reset();
    this.initForm();
  }

}
