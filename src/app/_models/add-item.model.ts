export interface AddUserModel {
    id: number;
    firstName: string;
    lastName: string;
    emailAddress: string;
    phoneNo: Number;
    address: string;
    zipCode: Number;
    countryCode: Number;
}
