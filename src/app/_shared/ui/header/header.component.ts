import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_auth/services/auth.service';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  navToggle: Boolean = false;

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit() {
  }

  toggleNav() {
    this.navToggle  = !this.navToggle;
  }

}
