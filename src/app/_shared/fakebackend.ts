import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError, EMPTY } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize, first } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    private fullName: String = 'Muhammad Asif';
    private email: String = 'asif-26@live.com';
    // updateArray = []
    body = {
        result: [
            {
                firstName: 'Muhammad',
                lastName: 'Asif',
                emailAddress: 'asif@abc.com',
                address: 'xyz Street',
                phoneNo: '+9213123',
                countryCode: '+92',
                zipCode: '234',
                id: 1
            },
            {
                firstName: 'Syed',
                lastName: 'Aslam',
                emailAddress: 'aslamf@abc.com',
                address: 'xyz Street',
                phoneNo: '+924353422',
                countryCode: '+92',
                zipCode: '343',
                id: 2
            },
            {
                firstName: 'Danish',
                lastName: 'Ahmed',
                emailAddress: 'Ahmed@abc.com',
                address: 'Abc Street',
                phoneNo: '+923452',
                countryCode: '+92',
                zipCode: '123',
                id: 3
            },
            {
                firstName: 'Muhammad',
                lastName: 'Asif',
                emailAddress: 'asif@abc.com',
                address: 'xyz Street',
                phoneNo: '+9213123',
                countryCode: '+92',
                zipCode: '234',
                id: 1
            },
            {
                firstName: 'Syed',
                lastName: 'Aslam',
                emailAddress: 'aslamf@abc.com',
                address: 'xyz Street',
                phoneNo: '+924353422',
                countryCode: '+92',
                zipCode: '343',
                id: 2
            },
            {
                firstName: 'Danish',
                lastName: 'Ahmed',
                emailAddress: 'Ahmed@abc.com',
                address: 'Abc Street',
                phoneNo: '+923452',
                countryCode: '+92',
                zipCode: '123',
                id: 3
            },
            {
                firstName: 'Muhammad',
                lastName: 'Asif',
                emailAddress: 'asif@abc.com',
                address: 'xyz Street',
                phoneNo: '+9213123',
                countryCode: '+92',
                zipCode: '234',
                id: 1
            },
            {
                firstName: 'Syed',
                lastName: 'Aslam',
                emailAddress: 'aslamf@abc.com',
                address: 'xyz Street',
                phoneNo: '+924353422',
                countryCode: '+92',
                zipCode: '343',
                id: 2
            },
            {
                firstName: 'Danish',
                lastName: 'Ahmed',
                emailAddress: 'Ahmed@abc.com',
                address: 'Abc Street',
                phoneNo: '+923452',
                countryCode: '+92',
                zipCode: '123',
                id: 3
            },{
                firstName: 'Muhammad',
                lastName: 'Asif',
                emailAddress: 'asif@abc.com',
                address: 'xyz Street',
                phoneNo: '+9213123',
                countryCode: '+92',
                zipCode: '234',
                id: 1
            },
            {
                firstName: 'Syed',
                lastName: 'Aslam',
                emailAddress: 'aslamf@abc.com',
                address: 'xyz Street',
                phoneNo: '+924353422',
                countryCode: '+92',
                zipCode: '343',
                id: 2
            },
            {
                firstName: 'Danish',
                lastName: 'Ahmed',
                emailAddress: 'Ahmed@abc.com',
                address: 'Abc Street',
                phoneNo: '+923452',
                countryCode: '+92',
                zipCode: '123',
                id: 3
            },




        ]
    };

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return of(null).pipe(mergeMap(() => {

            const auth = request.headers.get('Authorization');

            // Items list
            if (request.url.endsWith(environment['apiBaseUrl'] + '/api/items') && request.method === 'GET') {
                console.log('[ intercepting ] ' + request.method + ' : ' + request.url + ' ' + auth);
                this.body.result.push(request.body.result);
                return of(new HttpResponse({ status: 200, body: this.body }));
            }

            // Items list with pagination
            if (request.url.endsWith(environment['apiBaseUrl'] + '/api/item/pagination') && request.method === 'POST') {
                const payload = this.body.result
                const body = {
                    success: true,
                    result: payload.slice(parseInt((request.body.previous) + '0'), parseInt((request.body.current) + '0'))
                };
                return of(new HttpResponse({ status: 200, body: body }));
            }

            // Add User
            if (request.url.endsWith(environment['apiBaseUrl'] + '/api/item') && request.method === 'POST') {
                console.log('[ intercepting ] ' + request.method + ' : ' + request.url + ' ' + auth);
                const bodyPosted = request.body;
                const random = (Math.floor(Math.random() * Math.floor(1000)) + 1);
                const newResult = { ...bodyPosted, id: random };
                this.body.result.push(newResult);
                const body = {
                    success: bodyPosted ? true : false,
                    result: newResult
                };
                return of(new HttpResponse({ status: 200, body: body }));
            }

            // LOGIN
            if (request.url.endsWith(environment['apiBaseUrl'] + '/api/auth/login') && request.method === 'POST') {
                console.log('[ intercepting ] ' + request.method + ' : ' + request.url + ' ' + auth);
                const bodyPosted = request.body;

                let body = {};

                // simulating a valid/invalid login
                if (request.body.password === '12345') {
                    body = {
                        token: 'token_' + this.makeid(),
                        user: {
                            fullname: this.fullName,
                            email: this.email,
                            username: bodyPosted['username'],
                        }
                    };
                }
                return of(new HttpResponse({ status: 200, body: body }));
            }

            // VALIDATE TOKEN
            if (request.url.endsWith(environment['apiBaseUrl'] + '/api/auth/validate-token') && request.method === 'GET') {

                console.log('[ intercepting ] ' + request.method + ' : ' + request.url + ' ' + auth);
                const body = {
                    user: {
                        fullname: this.fullName,
                        email: this.email,
                        username: this.email,
                    }
                };
                return of(new HttpResponse({ status: 200, body: body }));
            }

            // Filter 
            if (request.url.startsWith(environment['apiBaseUrl'] + '/api/item/search') && request.method === 'POST') {
                console.log('[ intercepting ] ' + request.method + ' : ' + request.url + ' ' + auth);
                const payload = this.body.result;
                const result = payload.filter(item => {
                    return (item.firstName.toLowerCase().indexOf(request.body.searchKeys.toLowerCase()) > -1) 
                    || (item.lastName.toLowerCase().indexOf(request.body.searchKeys.toLowerCase()) > -1) 
                    || (item.emailAddress.toLowerCase().indexOf(request.body.searchKeys.toLowerCase()) > -1)
                })
                const body = {
                    success: true,
                    result: result
                };
                return of(new HttpResponse({ status: 200, body: body }));
            }

            // at default just process the request
            return next.handle(request);
        }))
            .pipe(materialize())
            .pipe(delay(500))
            .pipe(dematerialize());
    }

    // Fake data


    private makeid(): string {
        return 'helloWorld';
    }
}
