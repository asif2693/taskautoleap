import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';


import { Routes, RouterModule } from '@angular/router';
import { UsersModule } from '../users-module/users-module.module';
import { AddUserComponent } from '../users-module/add-user/add-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';




const routes: Routes = [
  { path: '', component: HomeComponent },
];

@NgModule({
  declarations: [HomeComponent,AddUserComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    FormsModule,

  ],
  exports: [RouterModule]
})
export class HomeModule { }
