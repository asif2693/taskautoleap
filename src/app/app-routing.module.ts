import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_auth/guards/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./login-module/login-module.module').then(m => m.LoginModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./home-module/home-module.module').then(m => m.HomeModule),
    canActivate: [AuthGuard] 
  },
  {
    path: 'users-list',
    loadChildren: () => import('./users-module/users-module.module').then(m => m.UsersModule)
  },
  { path: '**', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: '',  redirectTo: '/dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
