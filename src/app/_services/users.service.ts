import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject,throwError, of , BehaviorSubject} from 'rxjs';
import { map,mergeMap,switchMap ,catchError , tap} from 'rxjs/operators';

import { UserModel } from '../_models/user.model';

import { AddUserModel } from '../_models/add-item.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  // create a BehaviourSubject Observable with type UserModel[] and default value []
  items$ = new BehaviorSubject<UserModel[]>([]);

  constructor(
    private http: HttpClient,
  ) { }

  clear(): void {
    this.items$.next([]);
  }

  getAll(): UserModel[] {
    return this.items$.getValue();
  }


  getPaginatedResults(payloads): Observable<any> {
    return this.http.post(environment['apiBaseUrl'] + '/api/item/pagination' , payloads)
      .pipe(
        map(responseData => {
            return (responseData['success'] && responseData['success'] === true) ? responseData['result'] : false;
          }
        ),
        tap(item => { if (item) { this.addItem(item); }}), // when success, add the item to the local service
        catchError(err => {
          return of(false);
        }),
      );
  }

  getFilteredData(searchKeys): Observable<any> {
    return this.http.post(environment['apiBaseUrl'] + '/api/item/search' , {searchKeys})
      .pipe(
        map(responseData => {
            return (responseData['success'] && responseData['success'] === true) ? responseData['result'] : false;
          }
        ),
        tap(item => { if (item) { this.addItem(item); }}), // when success, add the item to the local service
        catchError(err => {
          return of(false);
        }),
      );
  }

  get(id: number): UserModel {
    const currentItems: UserModel[]  = this.getAll();
    if (currentItems.length === 0) {
      return null;
    }

    const index1  = currentItems.findIndex((element) => {
      return element.id === id;
    });
    return (index1 >= 0 && currentItems[index1]) ? currentItems[index1] : null;
  }

  add(payload: AddUserModel): Observable<any> {
    return this.http.post(environment['apiBaseUrl'] + '/api/item' , payload)
      .pipe(
        map(responseData => {
            return (responseData['success'] && responseData['success'] === true) ? responseData['result'] : false;
          }
        ),
        tap(item => { if (item) { this.addItem(item); }}), // when success, add the item to the local service
        catchError(err => {
          return of(false);
        }),
      );
  }

  addItem(item: UserModel): void {
    const currentItems: UserModel[]  = this.getAll();
    currentItems.push(item);
    this.items$.next(currentItems);
  }


  fetch(): Observable<any> {

    this.clear();

    return this.http.get(environment['apiBaseUrl'] + '/api/items')
      .pipe(
        map(data => {
            return (data['result']) ? data['result'] : false;
          }
        ),
        tap((items) => { if (items) { this.items$.next(items); }}),
        catchError(err => {
          return of(false);
        }),
      );
  }

}
